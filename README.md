# Parcial Elect. Desarrollo Web 2017-2
##### Profesor: Pablo Bejarano
##### Duración de la prueba 2:30m
#
#

###Antes de iniciar
La entrega de este examen se hará vía web mediante la subida a un repositorio al cual el docente con la dirección ***pabhoz@gmail.com*** tendrá acceso como administrador. Sólo serán revisados los commits realizados antes de las 9:30pm del día 30 de Agosto de 2017.

### Borrar .git
Recuerde borrar el folder .git del proyecto e inicializar uno nuevo para evitar inconvenientes con su repositorio.

### Recomendaciones

La prueba es individual.

Ested tendrá acceso a internet y a todo su código fuente desarrollado, están estrictamente prohibidos los sitios sociales o de comunicación, prestarse memorias o hablar; cualquier intento de fraude implicará la anulación de su prueba. Todos los cambios realizados al repositorio depués de las horas de entrega no serán calificados.

### La idea - ¿Qué hacer?

Usted va a desarrollar un App móvil para escuchar música por streaming basada en el contenido que el servidor web le provea, es decir, los usuarios sólo podran escuchar y crear listas de reproducción con la música disponible en el servidor. Apoyado en los servicios REST provistos usted definirá la lógica de negocio y las entidades para suplir las necesidades establecidas.

### Base

Dentro de éste repositorio se encuentran las bases para el desarrollo del proyecto. La carpeta StreamList contiene las bases para la aplicación y los serivcios web. co.edu.usbcali.streamlist es la base de una aplicación Ionic 3 (tabs) mientras que StreamListServer es la base de un proyecto de servicios web RESTful en PHP.

**Nota:** Las funcionalidades valen 0, 0.5 ó 1, es decir, nada, la mitad o toda.

### Del Proyecto...

Yo le proveeré su modelo de datos, usted debera abstraer las entidades del mismo. 

Se espera que en el App se creen proveedores para:
+ Song
+ Playlist
+ User

Por otro lado en el servidor los modelos a crear deberían ser:
+ Song
+ Playlist
+ Artist
+ Album
+ User

Y las lógicas de negocio que se proponen son:
+ Songs_bl
+ Playlists_bl
+ Users_bl

Recuerde user todos sus conocimientos para la correcta definición de dichas partes (POO).

### Aplicación (50%)

#### Pages

La aplicación cuenta con las siguientes pages y navegación:

+ Login
    + Registro
+ Home
    + SongsLD
    + PlaylistsLD
        + PlaylistLD
+ Search
    + SongsLD
    + PlaylistsLD
        + PlaylistLD
+ Profile (El perfíl no tendrá contenido para éste ejercicio)

**Nota:** Tenga en cuenta el siguiente look & feel de la aplicación.
+ ![Home](Samples/Home.jpg?raw=true "Home")
+ ![PlaylistsDT](Samples/PlaylistsDT.jpg?raw=true "Playlists Lista Detalle")
+ ![PlaylistDT](Samples/PlaylistDT.jpg?raw=true "Playlist Lista Detalle")
+ ![Search](Samples/Search.jpg?raw=true "Busqueda")
+ ![SongsDT](Samples/SongssDT.jpg?raw=true "Canciones Lista Detalle")

### Funcionalidades

1. Los usuarios podrán registrarse y logearse en el sistema
2. Los usuarios tienen canciones agregadas a sus librerías de música, esas canciones son las que fueron agregadas desde la busqueda a sus librerías. Así mismo pueden eliminar canciones de su librería.
3. Los usuarios podrán crear, editar y eliminar listas de reproducción a la que podrán agregar canciones de su librería.
4. Los usuarios podrán buscar canciones por artista, albúm o nombre y los resultados devueltos vendran filtrados por dichos criterios.

### Servicios (50%)

1. Desarrolle la lógica de negocios para realizar las consultas requeridas por la aplicación.
    + Songs_bl
    + Playlists_bl
    + Users_bl

**Nota:** Recuerde revisar y completar las fabricas e interfaces.

## Entrega

El proyecto debe ser subido a un repositorio en Bitbucket.

Muchos exitos.

# Recuerde

Para evitar pesos adicionales al proyecto el archivo base/.gitignore evitó que se subieran los "node_modules" recuerde entonces que usted debería usar el comando

`npm install`

para decargarlos y poder trabajar.

## Chrysalis 

Si usted no quiere hacer la construcción de los modelos desde cero puede usar [Chrysalis](https://github.com/pabhoz/Chrysalis/)