-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema streamlist
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `streamlist` ;

-- -----------------------------------------------------
-- Schema streamlist
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `streamlist` DEFAULT CHARACTER SET utf8 ;
USE `streamlist` ;

-- -----------------------------------------------------
-- Table `streamlist`.`Artist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `streamlist`.`Artist` ;

CREATE TABLE IF NOT EXISTS `streamlist`.`Artist` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `streamlist`.`Song`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `streamlist`.`Song` ;

CREATE TABLE IF NOT EXISTS `streamlist`.`Song` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` INT NOT NULL,
  `artist` INT NOT NULL,
  `src` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Song_Artist1`
    FOREIGN KEY (`artist`)
    REFERENCES `streamlist`.`Artist` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Song_Artist1_idx` ON `streamlist`.`Song` (`artist` ASC);


-- -----------------------------------------------------
-- Table `streamlist`.`Album`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `streamlist`.`Album` ;

CREATE TABLE IF NOT EXISTS `streamlist`.`Album` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `artist` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Album_Artist1`
    FOREIGN KEY (`artist`)
    REFERENCES `streamlist`.`Artist` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Album_Artist1_idx` ON `streamlist`.`Album` (`artist` ASC);


-- -----------------------------------------------------
-- Table `streamlist`.`Playlist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `streamlist`.`Playlist` ;

CREATE TABLE IF NOT EXISTS `streamlist`.`Playlist` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `user` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `streamlist`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `streamlist`.`User` ;

CREATE TABLE IF NOT EXISTS `streamlist`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `username_UNIQUE` ON `streamlist`.`User` (`username` ASC);


-- -----------------------------------------------------
-- Table `streamlist`.`Library`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `streamlist`.`Library` ;

CREATE TABLE IF NOT EXISTS `streamlist`.`Library` (
  `User_id` INT NOT NULL,
  `Song_id` INT NOT NULL,
  PRIMARY KEY (`User_id`, `Song_id`),
  CONSTRAINT `fk_User_has_Song_User`
    FOREIGN KEY (`User_id`)
    REFERENCES `streamlist`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Song_Song1`
    FOREIGN KEY (`Song_id`)
    REFERENCES `streamlist`.`Song` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_User_has_Song_Song1_idx` ON `streamlist`.`Library` (`Song_id` ASC);

CREATE INDEX `fk_User_has_Song_User_idx` ON `streamlist`.`Library` (`User_id` ASC);


-- -----------------------------------------------------
-- Table `streamlist`.`Album_x_Song`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `streamlist`.`Album_x_Song` ;

CREATE TABLE IF NOT EXISTS `streamlist`.`Album_x_Song` (
  `Album_id` INT NOT NULL,
  `Song_id` INT NOT NULL,
  `track_n` INT NOT NULL,
  PRIMARY KEY (`Album_id`, `Song_id`),
  CONSTRAINT `fk_Album_has_Song_Album1`
    FOREIGN KEY (`Album_id`)
    REFERENCES `streamlist`.`Album` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Album_has_Song_Song1`
    FOREIGN KEY (`Song_id`)
    REFERENCES `streamlist`.`Song` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Album_has_Song_Song1_idx` ON `streamlist`.`Album_x_Song` (`Song_id` ASC);

CREATE INDEX `fk_Album_has_Song_Album1_idx` ON `streamlist`.`Album_x_Song` (`Album_id` ASC);


-- -----------------------------------------------------
-- Table `streamlist`.`Playlist_x_Song`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `streamlist`.`Playlist_x_Song` ;

CREATE TABLE IF NOT EXISTS `streamlist`.`Playlist_x_Song` (
  `Song_id` INT NOT NULL,
  `Playlist_id` INT NOT NULL,
  PRIMARY KEY (`Song_id`, `Playlist_id`),
  CONSTRAINT `fk_Song_has_Playlist_Song1`
    FOREIGN KEY (`Song_id`)
    REFERENCES `streamlist`.`Song` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Song_has_Playlist_Playlist1`
    FOREIGN KEY (`Playlist_id`)
    REFERENCES `streamlist`.`Playlist` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Song_has_Playlist_Playlist1_idx` ON `streamlist`.`Playlist_x_Song` (`Playlist_id` ASC);

CREATE INDEX `fk_Song_has_Playlist_Song1_idx` ON `streamlist`.`Playlist_x_Song` (`Song_id` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
