<?php

/**
* Class Album generated by Chrysalis v.1.0.0
* 
* Chrysalis info:
* @author pabhoz
* github: @pabhoz
* bitbucket: @pabhoz
* 
*/ 
 
class Album extends BModel {

    private $id;
    private $name;
    private $artist;

    public function __construct( $id,  string $name, int $artist) {

        parent::__construct();
        $this->id = $id;
        $this->name = $name;
        $this->artist = $artist;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getArtist(): int {
        return $this->artist;
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function setName(string $name) {
        $this->name = $name;
    }

    public function setArtist(int $artist) {
        $this->artist = $artist;
    }

    public function getMyVars(){
        return get_object_vars($this);
    }

}