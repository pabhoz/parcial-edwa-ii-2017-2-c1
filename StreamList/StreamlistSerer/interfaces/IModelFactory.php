<?php

/**
 *
 * @author pabhoz
 */
interface IModelFactory {
    //TODO: Completar las definiciones
    public function newAlbum (): Album;
    public function newArtist (): Artist;
    public function newLibrary (): Library;
    public function newSong():Song;
    public function newUser (): User;
    
}
