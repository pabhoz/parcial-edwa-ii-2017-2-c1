<?php

class Index_controller extends BServiceController {

    function __construct() {
        parent::__construct();
    }
    
   
    public function getIndex() {
        Request::setHeader(202, "text/html");
        echo "Get method Index controller";
    }

    public function postIndex() {
        Request::setHeader(202, "text/html");
        echo "Post method Index controller"; 
    }

    public function putIndex() {
        $_PUT = $this->_PUT;
        Request::setHeader(202, "text/html");
        echo "Put method Index controller";
    }

    public function deleteIndex() {
        $_DELETE = $this->_DELETE;
        Request::setHeader(202, "text/html");
        echo "Delete method Index controller";
    }
}
